import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import vuetify from "./plugins/vuetify"

// Module augmentations for Vue
declare module 'vue/types/vue' {
  interface Vue {
    renderChart(chartData: Chart.ChartData, options?: Chart.ChartOptions): void
    addPlugin (plugin?: object): void
  }
}

Vue.config.productionTip = false

new Vue({
  name: "Dashboard Builder",
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app")
