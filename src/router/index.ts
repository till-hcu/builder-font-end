import Vue from "vue"
import VueRouter, { RouteConfig } from "vue-router"
import DashboardBuilder from "../views/DashboardBuilder.vue"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: "/",
    redirect: "/builder"
  },
  {
    path: "/builder",
    name: "Dashboard Builder",
    component: DashboardBuilder,
    meta: { title: 'Dashboard Builder' }
  },
  {
    path: "/dashboard",
    name: "Dashboard Preview",
    component: () => import("../views/GenericDashboard.vue"),
    meta: { title: 'Dashboard Preview' }
  },
]

const router = new VueRouter({
  routes
})

export default router
