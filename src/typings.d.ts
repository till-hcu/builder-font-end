import Chart from "chart.js";

declare global {
    interface GenericObject {
        [key: string]: any;
    }
    interface WPSResult {
      GroupByAttributes:  string[],
      AggregationResults: [][],
      AggregationFunctions: string[],
      AggregationAttribute: string
    }
    interface StoreState {
      config: {},
      sources: {
          layerConf?: GenericObject[],
          restConf?: GenericObject[],
          aggregationsConf?: GenericObject[],
      }
      storedQueries: {
        queries: {hash: string, query: GenericObject}[],
        selected: {hash: string, query: GenericObject}[]
      }
      storedFilters: {
        filters: {hash: string, query: ChartFilter}[],
        selected: {hash: string, query: ChartFilter}[]
      }
      aggregation: {
        attributes?: GenericObject,
        definition: GenericObject | null,
        requestBody: string | null,
        inputs: Input[],
        filters: []
      },
      results: WPSResult[],
      genericCharts: UDPChart[], // TODO move to own store
      genericFilters: ChartFilter[], // TODO move to own store
      chart: UDPChart,
      charts: UDPChart[]
      filters: ChartFilter[],
      executionTime: number
    }
    interface UDPChart {
      chartData: Chart.ChartData,
      chartOptions: GenericObject,
      chartType: Chart.ChartType,
      hash?: string
    }
    interface Input {
      name: string,
      dataSource: GenericObject,
      type: string,
      featureTypes: GenericObject,
      value: string,
      linked?: number
    }

    interface ChartFilter {
      key: string,
      name: string,
      type: "select" | "slider" | "map"
      cql?: string,
      items?:  string[],
      attribute: string,
      layerId?: string,
      layerAttribute?: string,
      multiple?: boolean,
      minmax?: [number, number],
      step?: number
    }

    var BACKEND_URL: string
    var GEOSERVER_LOCAL: boolean
}

export default null
