import { parseXml, reduceDescribeFeatureType } from "./parsing"
import Utils from "./utils"
import { Commit, Dispatch, Store } from "vuex"

/**
 * @todo Move to parsing?
 */
function processResults (result) {
    try {
        const _result = JSON.parse(result)
        this.commit("addResult", _result.aggregation)
        this.dispatch("loadChartConfig", _result.chart)
        this.dispatch("composeChartData", _result.aggregation)
    }
    catch (e) {
        console.warn("result cannot be parsed to JSON", e)
    }
}

/**
 * Get chart from backend.
 * @param {Commit} commit
 * @param {String} payload.hash
 * @param {Object} payload.body
 */
export async function getChartByHash({commit}, {hash, body}) {
    if (!hash) {
        console.warn("Missing hash to get data from backend")
        return
    }

    let url = BACKEND_URL + "/getChart" + `?hash=${hash}`

    const response = await fetch(url, {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body || {})
        }),
        result = await response.text();

    try {
        // add chart to the store
        const chart = JSON.parse(result);
        commit('genericCharts', chart)
    }
    catch (e) {
        console.error(e, "Chart could not be generated for the specified hash.")
    }
    
}

/**
 * Get graph information from backend.
 *
 * @param dispatch
 * @param hash
 */
export async function getFilterByHash({dispatch}, hash) {
    if (!hash) {
        console.warn("Missing hash to get filter from backend")
        return
    }

    const url = BACKEND_URL + "/getAllFilters"
    const response = await fetch(url)
    const allFilters = await response.json()

    // filter format return from backend ["yxz123": hashString, {}: filterObject]
    let requestedFilter = allFilters.filter(filter => { return filter[0] === hash })[0]
    requestedFilter[1]["hash"] = hash  // add hash to filter object
    requestedFilter = requestedFilter[1]

    // make chart and add it to the store
    dispatch("checkAndAddFilter", requestedFilter)
}


export async function describeFeatureType (service) {
    let url = GEOSERVER_LOCAL ? "http://localhost:8600/geoserver/ows" : service.url

    url += "?service=WFS"
    url += "&request=DescribeFeatureType"
    url += "&version=" + service.version
    url += (service.version > "1.1.0" ? "&typeNames=" : "&typeName=") + service.featureType

    const res = await fetch(url)
    const xml = await res.text()

    return parseXml(xml, reduceDescribeFeatureType, { featureType: service.featureType })
}

export async function fetchSchemas ({ state, commit }, sources: GenericObject[]) {
    let schema: GenericObject

    commit("aggregation/attributes", {}) // clean schemas

    for (const source of sources) {
        if (source?.typ === "WFS") {
            schema = await describeFeatureType(source)
            commit("aggregation/addAttributes", {
                [source.name]: schema
            })
        }
    }
}

export async function updateInputSchema ({state, getters}, input: Input) {
    let schema: GenericObject

    if (input.dataSource) {
        if (input.dataSource.typ === "WFS") {
            schema = await describeFeatureType(input.dataSource)
            if (input.type) {
                schema = Utils.dataSources.filterSchema(schema, input, "flag") // Don't filter, but disable irrelevant attributes
            }
            input.featureTypes = schema
        }
    }
}

export async function loadStoredQueries ({commit}) {
    const url = BACKEND_URL + "/getAllQueries"
    const response = await fetch(url)
    const storedQueries = await response.json()

    commit("storedQueries/queries", storedQueries.map(el => ({hash: el[0], query: el[1]})))
}

export async function loadStoredFilters ({commit}) {
    const url = BACKEND_URL + "/getAllFilters"
    const response = await fetch(url)
    const storedFilters = await response.json()

    commit("storedFilters/filters", storedFilters.map(el => ({hash: el[0], filter: el[1]})))
}

export async function executeQuery({state, commit, dispatch}, query) {
    let result: GenericObject | string
    const url = BACKEND_URL + "/executeQuery"
    let dt = new Date().getTime()

    const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(query)
    })

    dt = new Date().getTime() - dt
    commit("executionTime", dt)
    result = await response.text()
    processResults.call(this, result)
}

export async function saveFilter({dispatch}, filter: ChartFilter) {
    let url = BACKEND_URL + "/saveFilter"

    const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(filter)
    })
    const msg = await response.text()

    dispatch("loadStoredFilters")
    alert(msg)
}

export async function deleteFilter({dispatch}, hash: string) {
    let url = BACKEND_URL + "/deleteFilter"

    url += `?hash=${hash}`

    const response = await fetch(url)
    const msg = await response.text()

    dispatch("loadStoredFilters")
    alert(msg)
}

export async function saveQuery({dispatch}, query) {
    let url = BACKEND_URL + "/saveQuery"

    const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(query)
    })
    const msg = await response.text()

    dispatch("loadStoredQueries")
    alert(msg)
}

export async function getResults ({state, commit}, payload?: {hash: string, body: GenericObject}) {
    const _hash = payload?.hash || state.storedQueries.selected?.[0]?.hash || null
    const _body = payload?.body || {}
    let result: GenericObject | string
    let url = BACKEND_URL + "/getResults"
    let dt = new Date().getTime()

    if (_hash) {
        url += `?hash=${_hash}`
        const response = await fetch(url, {
            method: "POST",
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(_body)
        })
        dt = new Date().getTime() - dt
        commit("executionTime", dt)
        result = await response.text()
        processResults.call(this, result)
    }
}

export async function execute ({state, commit, dispatch}: {state: StoreState, commit: Commit, dispatch: Dispatch}, requestBody?: string) {
    const _requestBody = requestBody || state.aggregation.requestBody
    let url = GEOSERVER_LOCAL ? "http://localhost:8600/geoserver/ows" : state.aggregation.definition.url
    let result: GenericObject | string

    url += "?service=" + state.aggregation.definition.service
    url += "&request=" + state.aggregation.definition.request
    url += "&version=" + state.aggregation.definition.version

    const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "text/plain",
        },
        body: _requestBody
    })

    result = await response.text()
    processResults.call(this, result)
}

export default {
    execute,
    fetchSchemas,
    describeFeatureType,
    getChartByHash
}
