const state: StoreState = {
  config: {},
  sources: {},
  storedQueries: {
    queries: [],
    selected: []
  },
  storedFilters: {
    filters: [],
    selected: []
  },
  aggregation: {
    definition: null,
    requestBody: null,
    inputs: [],
    filters: []
  },
  results: [],
  genericCharts: [],
  genericFilters: [],
  chart: {
    chartData: null,
    chartOptions: {
      dataset: {},
      options: {}
    },
    chartType: "bar"
  },
  charts: [],
  filters: [],
  executionTime: 0
}

export default state
