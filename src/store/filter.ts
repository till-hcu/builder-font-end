export async function checkAndAddFilter ({state, commit}, filter) {
  if (state.genericFilters.some((genFilter) => {  return genFilter.hash === filter.hash })) {
    // filter already exists in generic filters. No need to add it. Return.
    return
  }

  commit('genericFilters', filter)
}

