import Jsonix from "jsonix"
import ogcSchemas from "ogc-schemas"
import w3cSchemas from "w3c-schemas"

// PARSERS
const context = new Jsonix.Context([
    w3cSchemas.XSD_1_0,
    w3cSchemas.XLink_1_0,
    ogcSchemas.WFS_1_1_0,
    ogcSchemas.WFS_2_0,
    ogcSchemas.Filter_1_1_0,
    ogcSchemas.Filter_2_0,
    ogcSchemas.OWS_1_0_0,
    ogcSchemas.OWS_1_1_0,
    ogcSchemas.OWS_2_0,
    ogcSchemas.GML_3_1_1,
    ogcSchemas.SMIL_2_0_Language,
    ogcSchemas.SMIL_2_0,
    ogcSchemas.WPS_1_0_0,
    ogcSchemas.WPS_2_0
])
const parser = context.createUnmarshaller()
const serializer = context.createMarshaller()

export function reduceDescribeFeatureType (json, opts = { featureType: null }) {
    const featureType = opts.featureType
    let _json: GenericObject

    if (!json.value.complexType) {
        _json = json.value.element

        if (featureType) {
            _json = _json.filter(el => featureType.includes(el.name))
        }
        _json = _json.map(el => ({
            name: el.name,
            attributes: el.complexType?.complexContent?.extension?.sequence?.element?.map(attr => attr.otherAttributes)
        }))
    } else {
        _json = json.value?.complexType

        _json = _json.map(el => ({
            name: el.name,
            attributes: el.complexContent?.extension?.sequence?.element?.map(attr => attr.otherAttributes)
        }))
    }

    return _json
}

export function parseXml (xml: string, reducer?: Function, opts?: GenericObject) {
    let _json = parser.unmarshalString(xml)

    if (reducer) {
        _json = reducer(_json, opts)
    }
    return _json
}

export default {
    parseXml,
    reduceDescribeFeatureType
}
