import sources from "@/config/sources.json"
import Utils from "./utils"

export async function loadConfig ({ commit }) {
    const _sources = {}
    let res: Response, json: JSON, filteredJson: JSON

    for (const key in sources) {
        try {
            res = await fetch(sources[key])
            json = await res.json()
            filteredJson = Utils.config.filterDataSources(key, json)
            _sources[key] = filteredJson

            commit("addSource", { [key]: filteredJson })
        } catch (e) {
            console.warn(`${key} from ${sources[key]} could not be read. Maybe the file doesn't exist or is not readable`)
        }
    }

    return _sources
}

export async function loadRequestBody ({ commit }, filename) {
    const url = "./xml/" + filename
    const res = await fetch(url)
    const body = await res.text()

    commit("aggregation/requestBody", body)
}

export default {
    loadConfig,
    loadRequestBody,
    sources
}
