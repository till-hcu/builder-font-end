import { Dispatch } from "vuex"
import Utils from "./utils/index"

function generateConfigJson (state: StoreState) {
    return {
        hashes: {
            charts: state.storedQueries.selected.map(query => query.hash),
            filters: state.storedFilters.selected.map(filter => filter.hash),
            layout: {} // placeholder
        }
    }
}

export function exportDashboard ({state, dispatch}: {state: StoreState, dispatch: Dispatch}) {
    const config = generateConfigJson(state)
    Utils.downloadObjectAsJson(config, "config")
}
export function previewDashboard ({state, dispatch}: {state: StoreState, dispatch: Dispatch}) {
    console.log(state.storedFilters.selected, state.storedQueries.selected)
}

export default {
    exportDashboard,
    previewDashboard
}