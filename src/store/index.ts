import Vue from "vue"
import Vuex from "vuex"
import Config, { loadConfig, loadRequestBody } from "./config"
import state from "./state"
import Requests, { fetchSchemas, updateInputSchema, execute, executeQuery, saveQuery, loadStoredQueries, getResults, loadStoredFilters, saveFilter, deleteFilter, getChartByHash, getFilterByHash } from "./requests"
import { composeChartData, loadChartConfig, addChart, updateChart, rmChart } from "./charts"
import { prepareExecution, checkLinkedInputs } from "./aggregations"
import { generateMutations } from "./utils/generators"
import { exportDashboard, previewDashboard } from "./dashboard"
import { ChartType } from "chart.js"
import { checkAndAddFilter } from "./filter"

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  getters: {
    _dataSources: s => [
      ...s.sources.layerConf || []
    ],
    _aggregations: s => s.sources.aggregationsConf || [],
    _attributes: s => s.aggregation?.inputs?.map(i => i.value) || []
  },
  mutations: {
    ...generateMutations(state),
    addSource (state: StoreState, payload) {
      state.sources = {
        ...state.sources,
        ...payload
      }
    },
    addResult (state: StoreState, payload: WPSResult) {
      state.results.push(payload)
    },
    "aggregation/definition" (state, payload: GenericObject) {
      state.aggregation.definition = payload

      const inputs = payload.inputs?.map(input => ({
        dataSource: null,
        featureTypes: [],
        value: null,
        ...input
      })) || null

      const filters = payload.filters?.map(filter => ({
        value: null,
        ...filter
      })) || null

      this.commit("aggregation/inputs", inputs)
      this.commit("aggregation/filters", filters)
      this.dispatch("loadRequestBody", payload.requestBody)
    },
    "aggregation/dataSources" (state, payload: GenericObject[]) {
      state.aggregation.dataSources = payload
      this.dispatch("fetchSchemas", payload)
    },
    "aggregation/addAttributes" (state, payload: GenericObject) {
      state.aggregation.attributes = {
        ...state.aggregation.attributes,
        ...payload
      }
    },
    "aggregation/rmAttributes" (state, payload: string) {
      delete state.aggregation.attributes[payload]
    },
    "aggregation/inputs/updateDataSource" (state, {dataSource, index}) {
      state.aggregation.inputs[index].dataSource = dataSource;
    },
    "chart/chartType" (state: StoreState, payload: ChartType) {
      state.chart.chartType = payload
      state.chart.chartOptions.dataset = {}
      state.chart.chartOptions.options = {}
    },
    "genericCharts" (state: StoreState, payload: UDPChart) {
      let genericCharts = state.genericCharts
      const existingIndex = genericCharts.findIndex((chart) => chart.hash === payload.hash);

      if (existingIndex !== -1) {
        genericCharts[existingIndex] = payload
      }
      else {
        genericCharts.push(payload)
      }

      state.genericCharts = [...genericCharts]
    },
    "genericFilters" (state: StoreState, payload: ChartFilter) {
      state.genericFilters.push(payload)
    },
    "addfilter" (state: StoreState, newFilter: ChartFilter) {
      const oldFilter = state.filters.find((filter, i) => {
        if (filter.key === newFilter.key) {
          state.filters[i] = {...newFilter}
          return true
        }
        return false
      })

      if (!oldFilter) {
        state.filters.push(newFilter)
      }
      else {
        state.filters = [...state.filters]
      }
    },
    addChart,
    updateChart,
    rmChart
  },
  actions: {
    checkAndAddFilter,
    getChartByHash,
    getFilterByHash,
    exportDashboard,
    previewDashboard,
    executeQuery,
    saveQuery,
    saveFilter,
    deleteFilter,
    getResults,
    composeChartData,
    execute,
    prepareExecution,
    loadConfig,
    loadRequestBody,
    loadStoredQueries,
    loadStoredFilters,
    loadChartConfig,
    fetchSchemas,
    updateInputSchema,
    checkLinkedInputs,
    updateDataSource({dispatch, state}, {input, index}) {
      dispatch("updateInputSchema", input)
      dispatch("checkLinkedInputs", {updatedInput: input, updatedIndex: index})
    },
    initialize ({dispatch}) {
      console.info("Initializing...")
      console.info("the geoserver is running locally: " + GEOSERVER_LOCAL)
      console.info("the backend is available at " +  BACKEND_URL)

      dispatch('loadConfig')
      dispatch('loadStoredQueries')
      dispatch('loadStoredFilters')
    },
  },
  modules: {
  }
})
