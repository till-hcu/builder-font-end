import { ChartType } from "chart.js";
import { Commit } from "vuex";
import Utils from "./utils"

let _backgroundColors: string | string[]

function resetTempValues () {
    _backgroundColors = undefined
}

function getXLabels (result: WPSResult, separator = "/") {
    const indices = result.GroupByAttributes.length
    return result.AggregationResults.map(el => el.slice(0, indices).join(separator))
}

function getYLabel (label: string, result: WPSResult, separator = "/") {
    return result.AggregationAttribute + separator + label
}

function getBarDataStyles (data: number[], datasetCount: number, datasetIndex?: number, customColors: string[] = []) {
    if (datasetCount === 1) {
        return data.map((v,i) => {
            if (customColors.length === 1) {
                return customColors[0]
            }
            if (customColors.constructor === Object) {
                return customColors[v] || Utils.chart.getRandomColor()
            }
            return customColors[i] || Utils.chart.getRandomColor()
        })
    }
    else {
        return customColors[datasetIndex] || Utils.chart.getRandomColor()
    }
}

function getPieDataStyles (data: number[], datasetCount: number, datasetIndex?: number, customColors: string[] = []) {
    const backgroundColors = _backgroundColors || data.map((v,i) => {
        if (customColors.constructor === Object) {
            return customColors[v] || Utils.chart.getRandomColor()
        }
        return customColors[i] || Utils.chart.getRandomColor()
    })

    _backgroundColors = backgroundColors
    return backgroundColors
}

function getDataStyles (data: number[], datasetCount: number, datasetIndex: number, chartType: ChartType, opts: GenericObject) {
    let backgroundColor: string | string[] = []
    const datasetOpts = {}

    switch (chartType) {
        case "bar":
            backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor)
            break
        case "line":
            backgroundColor = getBarDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor)
            break
        case "pie":
            backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor)
            break
        case "polarArea":
            backgroundColor = getPieDataStyles(data, datasetCount, datasetIndex, opts.backgroundColor)
            break
    }

    return {
        ...opts,
        backgroundColor
    }
}

function getDatasets (result: WPSResult, chartType: ChartType = "bar", datasetOptions?: GenericObject) {
    const opts = datasetOptions || {}
    const cullIndices = result.GroupByAttributes.length
    return result.AggregationFunctions.map((_label, i) => {
        const label = getYLabel(_label, result)
        const data = result.AggregationResults.map(el => el[cullIndices + i])
        return {
            label,
            data,
            ...getDataStyles(data, result.AggregationFunctions.length, i, chartType, opts)
        }
    })
}

export function composeChartData ({state, commit}: {state: StoreState, commit: Commit}, result: WPSResult) {
    const chartType = state.chart.chartType
    const chartData = {
        labels: getXLabels(result),
        datasets: getDatasets(result, state.chart.chartType, state.chart.chartOptions.dataset)
    }
    
    resetTempValues()
    commit("chart/chartData", chartData)
    commit("addChart")
}

export function addChart(state) {
    state.charts.push({...state.chart})
}
export function updateChart(state, index) {
    state.charts[index] = state.chart
}
export function rmChart(state, index) {
    state.charts = [...state.charts.slice(0, index), ...state.charts.slice(index + 1, state.charts.length)]
}

export function loadChartConfig ({commit}, chartConfig) {
    commit("chart/chartType", chartConfig.chartType)
    commit("chart/chartOptions", chartConfig.chartOptions)
}

export default {
    composeChartData
}