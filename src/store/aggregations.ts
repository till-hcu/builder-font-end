/**
 * @description names of keys used for local processing only to exclude before making a request
 * @property {String[]} inputs - the keys to exlude from the inputs
 * @property {String[]} inputs - the keys to exlude from the filters
 */
const excludedKeys = {
    inputs: ["featureTypes"],
    filters: []
}

export function sanitizeQuery (aggregation) {
    const query = { ...aggregation.definition }

    query.filters = [...(aggregation.filters || [])]
    query.inputs = [...(aggregation.inputs || []).map(input => {
        const sanitizedInput = {}

        for (const key in input) {
            if (!excludedKeys.inputs.includes(key)) {
                sanitizedInput[key] = input[key]
            }
        }

        return sanitizedInput
    })]

    return query
}

export function prepareExecution ({ state, commit, dispatch }, action: "executeQuery" | "execute" | "saveQuery") {
    const query = {
        aggregation: sanitizeQuery(state.aggregation),
        chart: {
            chartOptions: state.chart.chartOptions,
            chartType: state.chart.chartType
        }
    }

    dispatch(action, query)
}

export function checkLinkedInputs({ state, dispatch, commit }, { updatedInput, updatedIndex }) {
    state.aggregation.inputs.forEach((_input: Input, i: number) => {
        if (i !== updatedIndex) {
            if (_input.linked === updatedIndex) {
                commit("aggregation/inputs/updateDataSource", { dataSource: updatedInput.dataSource, index: i })
                dispatch("updateInputSchema", _input)
            }
        }
    })
}

export default {
    prepareExecution,
    checkLinkedInputs,
}
