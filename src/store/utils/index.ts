import dataTypes from "@/config/data_types.json"

export default {
    downloadObjectAsJson (exportObj, exportName) {
        const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
        const downloadAnchorNode = document.createElement("a");

        downloadAnchorNode.setAttribute("href",     dataStr);
        downloadAnchorNode.setAttribute("download", exportName + ".json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    },
    string: {
        isUpperCase (char) {    
            return (char >= 'A') && (char <= 'Z');
        },
        beautifyString (str) {
            return str.split("").reduce((prettier, char, i) => {
                if (i === 0) {
                    return char.toUpperCase();
                }
                if (this.isUpperCase(char)) {
                    return prettier += ` ${char}`;
                }
                return prettier += char
            }, "");
        }
    },
    config: {
        filterDataSources(key, val) {
            if (key === "layerConf") {
                return this.filterLayerConf(val)
            }
            return val
        },
        filterLayerConf (layerConf: GenericObject[], include = { typ: ["WFS"] }) {
            const data = layerConf.filter(l => {
                for (const key in include) {
                    if (include[key].includes(l[key])) {
                        return true
                    }
                }
                return false
            })
            return data
        }
    },
    dataSources: {
        filterSchema(featureTypes: GenericObject, input: Input, action?: string) {
            const _featureTypes = Array.isArray(featureTypes) ? featureTypes : [featureTypes]

            for (const schema of _featureTypes) {
                switch (action) {
                    case "flag":
                        schema.attributes.forEach(attr => {
                            if (!this.checkAttributeType(attr, input)) {
                                attr.disabled = true
                            }
                        })
                        break
                    default:
                        schema.attributes = schema.attributes.filter(this.checkAttributeType.bind(input))
                }
            }

            return _featureTypes
        },
        checkAttributeType (attr: {name: string; type: string}, input?: Input): boolean {
            const _input = input.constructor === Object ? input : this // provide context through THIS-binding or as separate argument, usable as comparator
            const _typeArr = attr.type.split(":")
            const _type = _typeArr[_typeArr.length - 1]

            if (_input.type === "any") {
                return true
            }

            if (dataTypes[_input.type].includes(_type)) {
                return true
            } else if (_input.type === "geometry") {
                for (const ns of dataTypes.geometry) {
                    if (attr.type.includes(ns)) {
                        return true
                    }
                }
            }
            return false
        },
        sanitizeFeatureNamespace (featureNS: string) {
            if (featureNS?.includes("/")) {
                const _featureNS = featureNS.split("/")
                return _featureNS[_featureNS.length - 1]
            }
            return featureNS
        }
    },
    chart: {
        getRandomColor (clamp = [100, 255], alpha = 1.0) {
            const r = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0])
            const g = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0])
            const b = Math.round((Math.random() * (clamp[1] - clamp[0])) + clamp[0])
            return `rgba(${r}, ${g}, ${b}, ${alpha})`
        },
        sanitizeChartOptions (chartOptions) {
            const _chartOptions = {}
            for (const opt in chartOptions) {
                if (chartOptions[opt].constructor === Object) {
                    _chartOptions[opt] = this.sanitizeChartOptions(chartOptions[opt])
                }
                else {
                    if (Array.isArray(chartOptions[opt]) && chartOptions[opt].length > 0) {
                        _chartOptions[opt] = chartOptions[opt].map(el => isNaN(parseFloat(el)) ? el : parseFloat(el))
                        if (_chartOptions[opt].length === 1) {
                            _chartOptions[opt] = _chartOptions[opt][0]
                        }
                    }
                    else if (!isNaN(parseFloat(chartOptions[opt]))) {
                        _chartOptions[opt] = parseFloat(chartOptions[opt])
                    }
                    else {
                        _chartOptions[opt] = chartOptions[opt]
                    }
                }
            }
            return _chartOptions
        }
    }
}
