# dashboard-builder-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Docker
```
docker build -t dashboard-builder/docker-vue-frontend .
```
```
docker run -it -p 8080:8080 --rm --name docker-vue-frontend-1 dashboard-builder/docker-vue-frontend
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Beschreibung des builder-frontends

Teile dieser Dokumentation sind der schriftlichen Ausarbeitung der Machbarkeitsstudie Dashboard-Builder entnommen.
Diese befindet sich in folgendem Repository: https://bitbucket.org/till-hcu/machbarkeitsstudie/src/master/

Das Admin-Tool zur Konfiguration der Filter, Aggregationen und Charts ist analog zum Admin-Tool für das Masterportal zu verstehen. Wie in Kapitel 3.1.2 beschrieben ist davon auszugehen, dass jenes sich entsprechend erweitern lässt. Für den Prototyp wurde jedoch darauf verzichtet, da die Umstrukturierung des bestehenden Tools sich als aufwendiger herausgestellt hat, als Eingangs erwartet. Das Admin-Tool wurde in Vue.js und Typescript umgesetzt und kann enssprechend einfach lokal deployed werden. Dem Repository liegt ebenfalls eine Docker-Konfigurationsdatei bei. Das Tool funktioniert nur in Kombination mit dem Backend (s. 4.2.4), da die Verarbeitung der Daten und das Speichern von Konfigurationen dort vorgenommen werden .
Der Prototyp besteht aus zwei Hauptansichten: (1) Dem Builder und (2) der Vorschau, wobei erstere die Konfiguration von Aggregationen, Charts und Filtern über ein klassisches Formular GUI erlaubt und letztere den Anwendungsfall des über URL-Parameter aufgerufenen Dashboards illustriert. Die Vorschau-Ansicht wird separat in Kapitel 4.2.5 beschrieben, da sie letztendlich unabhängig funktionieren soll und nur für das Prototyping mit im Admin-Tool untergebracht wurde.
Prinzipiell werden alle im Builder konfigurierbaren Parameter über Online-Ressourcen abgerufen. Dazu werden, analog zum Masterportal, die URLs der verfügbaren Optionen in einer Konfigurationsdatei (/src/config) festgehalten und bei Aufruf der Website geladen. Dies umfasst (1) die Layer-Liste (services.json) und (2) die Liste der verfügbaren WPS-Operationen (aggregations.json, vgl. 4.2.1). Die auswählbaren Optionen für Charts und Filter sind in der Konfiguration selbst untergebracht, da davon ausgegangen wird, dass diese nicht regelmäßig erweitert oder upgedatet werden müssen.
Der Builder bietet folgende Komponenten:

1.	Aggregation-Builder: Hier kann eine Aggregation aus der Liste ausgewählt und mit Datenquellen, Attributen und Default-Filter konfiguriert werden. Die Abfrage des Daten-Schemas eines gewählten Layers (aus der services.json) findet über WFS DescribeFeatureType Requests statt um der NutzerIn die Auswahl der Attribute einer Datenquelle zu erlauben. Anschließend kann die Aggregation ausgeführt werden (Ein Vorschau-Chart wird erzeugt) und in der Datenbank gespeichert werden. Ein Freitext- oder Upload-Feld für Rohdaten ist nicht implementiert.
2.	Gespeicherte Queries: Hier werden die in der Datenbank gespeicherten Aggregationskonfigurationen tabellarisch und mit ihrem Hash angezeigt. Die Liste wird über die Backend-Route /getAllQueries abgerufen. Sie können von hier ausgewählt, ausgeführt und exportiert werden.
3.	Chart Builder: Der Chart-Builder erlaubt die Konfiguration von Charts (nur visuell, nicht datenseitig) mit einer Reihe von Optionen, die chart-js vorgibt und die in der /src/config/chart_options.json festgehalten sind (Nicht alle Chart-Typen sind umfassend implementiert oder gestestet worden). Von hieraus kann das Chart für die zuletzt ausgeführte Aggregation neu aufgebaut werden. Die Chart-Optionen werden beim Speichern der Aggregationskonfiguration (1.) mit abgelegt.
4.	Filter Builder: Hier können Filter für die Dashboards konfiguriert werden. Im Prototyp können „Select“, „Slider“ und „Map“ filter konfiguriert werden. Bei Select handelt es sich um ein schlichtes Dropdown-Menü, aus dem die inkludierten Werte aus einer Liste gewählt werden (z.B. Bezirke), der Slider ist ein Schieberegler für numerische Werte, und der Map Filter beschreibt eine Interaktion mit der Masterportal-Karte (s. Kapitel 4.2.6). Die einzugebenen Werte sind in der /src/config/filter_options.json definiert.
5.	Gespeicherte Filter: Analog zu 2. Filter werden über die Route /getAllFilters geladen.
6.	Chart Vorschau: Die Vorschau eines einzigen Charts zum Test der Optionen und Filter vor dem Speichern der Konfiguration.
7.	Export: Über den Export Button kann die vollständige Zusammenstellung von Filter-, Aggregations-, und Charts als JSON heruntergeladen werden. Der Vorschau-Button öffnet das Dashboard mit den gewählten Filter- und Chart-Hashes als URL Parametern.

## Architecture

Link to Schema: https://miro.com/app/board/o9J_khFZLbE=/

![Architecture_Schema](https://bitbucket.org/till-hcu/builder-back-end/downloads/architektur.png)