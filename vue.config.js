const webpack = require('webpack');

module.exports = {
  transpileDependencies: [
    "vuetify"
  ],
  devServer: {
    overlay: {
      warnings: false,
      errors: false
    },
    // port: 4545
  },
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        GEOSERVER_LOCAL: JSON.parse(JSON.stringify(process.env.GEOSERVER_LOCAL || false)),
        BACKEND_URL: JSON.stringify(process.env.BACKEND_URL || "http://localhost:5000")
      })
    ]
  },
  chainWebpack: config => {
    config.module
      .rule("jsonix")
      .test(require.resolve("jsonix"))
      .use("imports-loader")
      .loader("imports-loader?type=commonjs&additionalCode=var%20require%20=%20null")
      .end()
      .use("exports-loader")
      .loader("exports-loader?type=commonjs&exports=single|Jsonix")
      .end()
  }
}
