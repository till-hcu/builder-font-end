module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/essential",
    "@vue/standard",
    "@vue/typescript/recommended"
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    quotes: ["warn", "double"],
    "comma-dangle": "off",
    "brace-style": "off",
    'vue/valid-v-slot': ['error', {
      allowModifiers: true,
    }],
  },
  overrides: [
    {
      files: ["*.vue", "*.ts"],
      rules: {
        indent: "off"
      }
    }
  ]
}
